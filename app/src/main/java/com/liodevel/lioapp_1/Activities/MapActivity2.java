package com.liodevel.lioapp_1.Activities;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.ShareEvent;
import com.liodevel.lioapp_1.Objects.Track;
import com.liodevel.lioapp_1.Objects.TrackPoint;
import com.liodevel.lioapp_1.R;
import com.liodevel.lioapp_1.Utils.ScreenshotUtil;
import com.liodevel.lioapp_1.Utils.Server;
import com.liodevel.lioapp_1.Utils.Utils;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.views.MapView;
import com.orm.SugarContext;


import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;
import okhttp3.internal.Util;

public class MapActivity2 extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Boolean exit = false;
    SharedPreferences prefs;

    // VIEW
    private TextView startButton, textProviderInfo, textDistanceInfo, infoLocationLat, infoLocationLng;
    private Menu actionBarMenu;
    private Context context;
    private Chronometer chronoTrack;
    private MenuItem vehicleSpinner;

    private TextView chartBlack, chartRed, chartOrange, chartYellow, chartGreen, chartDarkGreen, chartBlue, chartCyan, chartMagenta;
    private LinearLayout leyenda1, leyenda2, leyenda3, leyenda4, leyenda5, leyendaColores, layoutInfoLocation, layoutInfo;
    private RelativeLayout layoutToShare;


    // MAPS
    private boolean centerMap = true;

    private MapView mapView = null;
    private MarkerOptions markerOptions;
    private Marker marker;
    private String mapStyle = Style.MAPBOX_STREETS;

    // TRACKING
    private long currentTrackObjectId = -1;
    private Track currentTrack = null;
    private Location prevLocation;
    private Location lastLocation;
    private float currentTrackDistance = 0;
    private float trackPointDistance = 0;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Date lastTrackPointDate = new Date();
    TrackPoint previousTr = new TrackPoint();
    private int vehicle = 1;


    // FLAGS
    private boolean tracking = false;
    private boolean trackerReady = false;

    private boolean gps_enabled = false;
    private boolean network_enabled = false;

    // TIMER
    private Timer timer;
    private TimerTask timerTask;
    private final Handler handler = new Handler();
    int counterTask = 0;
    long secondsTracking = 0;
    long startTimemillis;
    long currentTimemillis;

    // PREFERENCIAS POR DEFECTO
    private boolean onlyGPS = true;
    private int saveFrequency= 5;
    private String systemOfMeasurement = "metric";
    private boolean showLocation = true;

    // NOTIFICACION
    NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.logInfo("-------onCreate()");

        try {
            Fabric.with(this, new Crashlytics());
        } catch (Exception e){
            Utils.logInfo("Fabric enabled");
        }

        getDelegate().installViewFactory();
        getDelegate().onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map2);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        context = this;

        changeNotificationBar();

        try {
            SugarContext.init(context);
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        // ToolBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.liodevel_white)));

        // Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.bringToFront();
        navigationView.requestLayout();
        navigationView.setNavigationItemSelectedListener(this);

        // Shared Preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            onlyGPS = prefs.getBoolean("only_gps", true);
            showLocation = prefs.getBoolean("show_location", true);
            vehicle = prefs.getInt("vehicle", 1);
            mapStyle = prefs.getString("map_style", Style.MAPBOX_STREETS);

            Utils.logInfo("-PREFS- vehicle : " + vehicle);
            Utils.logInfo("-PREFS- mapStyle: " + mapStyle);

        }catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        try {
            systemOfMeasurement = prefs.getString("system_of_measurement", "metric");
            Utils.logInfo("-PREFS- SOM : " + systemOfMeasurement);

        }catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        try {
            int tempSaveFrequency = Integer.parseInt(prefs.getString("save_frequency", "5"));
            if (tempSaveFrequency < 5){
                saveFrequency = 5;
            } else if (tempSaveFrequency > 60){
                saveFrequency = 60;
            } else {
                saveFrequency = tempSaveFrequency;
            }

        } catch (Exception e){
            Crashlytics.logException(e);
            saveFrequency = 5;
        }

        Utils.logInfo("Pref_save_frequency: " + saveFrequency);


        // NOTIFICACION
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.ic_stat_icono_notificaciones);
        mBuilder.setOngoing(true);
        mBuilder.setContentTitle(getResources().getString(R.string.getting_location));
        mBuilder.setContentText("");
        mBuilder.setPriority(NotificationCompat.PRIORITY_MAX);

        Intent resultIntent = new Intent(this, MapActivity2.class);
        resultIntent.setAction(Intent.ACTION_MAIN);
        resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
        mBuilder.setContentIntent(pendingIntent);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(1 , mBuilder.build());


        // Inicializar mapa
        mapView = (MapView) findViewById(R.id.map);
        try {
            mapView.setAccessToken(getString(R.string.com_mapbox_mapboxsdk_accessToken));
        } catch (Exception e){
            Crashlytics.logException(e);
        }
            mapView.setStyle(Style.MAPBOX_STREETS);
        mapView.onCreate(savedInstanceState);


        try {
            initMapBox();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        // Infos
        startButton = (TextView) findViewById(R.id.text_info);
        startButton.setBackgroundColor(ContextCompat.getColor(this, R.color.liodevel_dark_grey));
        textProviderInfo = (TextView) findViewById(R.id.text_provider_info);
        //textTimeInfo = (TextView) findViewById(R.id.text_time_info);
        textDistanceInfo = (TextView) findViewById(R.id.text_distance_info);
        if (systemOfMeasurement.equals("metric")){
            textDistanceInfo.setText(getResources().getString(R.string.distance_metric));
        } else {
            textDistanceInfo.setText(getResources().getString(R.string.distance_imperial));
        }

        infoLocationLat = (TextView) findViewById(R.id.info_location_lat);
        infoLocationLng = (TextView) findViewById(R.id.info_location_lng);
        layoutInfo = (LinearLayout) findViewById(R.id.map_layout_info);
        layoutInfoLocation = (LinearLayout) findViewById(R.id.map_layout_location);
        showLocation(showLocation);

        chronoTrack = (Chronometer) findViewById(R.id.chronoTracking);

        startButton.setText(getResources().getString(R.string.getting_location));
        startButton.setTextSize(14);

        // Velocidades
        chartBlack = (TextView) findViewById((R.id.speed_black));
        chartRed = (TextView) findViewById((R.id.speed_red));
        chartOrange = (TextView) findViewById((R.id.speed_orange));
        chartYellow = (TextView) findViewById((R.id.speed_yellow));
        chartGreen = (TextView) findViewById((R.id.speed_green));
        chartDarkGreen = (TextView) findViewById((R.id.speed_dark_green));
        chartBlue = (TextView) findViewById((R.id.speed_blue));
        chartCyan = (TextView) findViewById((R.id.speed_cyan));
        chartMagenta = (TextView) findViewById((R.id.speed_magenta));

        leyenda1 = (LinearLayout) findViewById(R.id.map_leyenda_1);
        leyenda2 = (LinearLayout) findViewById(R.id.map_leyenda_2);
        leyenda3 = (LinearLayout) findViewById(R.id.map_leyenda_3);
        leyenda4 = (LinearLayout) findViewById(R.id.map_leyenda_4);
        leyenda5 = (LinearLayout) findViewById(R.id.map_leyenda_5);
        leyendaColores = (LinearLayout) findViewById(R.id.map_leyenda_colores);

        layoutToShare = (RelativeLayout) findViewById((R.id.map_layout_to_share));

        updateGpsProviders();
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Utils.logInfo("Nav item: " + id);

            // MY TRACKS
         if (id == R.id.nav_my_tracks && !tracking) {

             Intent launchNextActivity;
             launchNextActivity = new Intent(MapActivity2.this, MyTracksActivity.class);
             launchNextActivity.putExtra("favorites", "0");
             try {
                 locationManager.removeUpdates(locationListener);
             } catch (Exception e) {
             }
             DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
             drawer.closeDrawer(GravityCompat.START);
             startActivity(launchNextActivity);
             return true;

             // MY FAVORITE TRACKS
         }else if (id == R.id.nav_my_favorite_tracks && !tracking) {

             Intent launchFavoritesActivity;
             launchFavoritesActivity = new Intent(MapActivity2.this, MyTracksActivity.class);
             launchFavoritesActivity.putExtra("favorites", "1");
             try {
                 locationManager.removeUpdates(locationListener);
             } catch (Exception e){}
                 DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                 drawer.closeDrawer(GravityCompat.START);
                 startActivity(launchFavoritesActivity);
                 return true;

             // SETTINGS
        } else if (id == R.id.nav_settings) {
             //
             Answers.getInstance().logCustom(new CustomEvent("Settings"));
             //
             Intent launchSettingsActivity;
             launchSettingsActivity = new Intent(MapActivity2.this, SettingsActivity.class);
             try {
                 locationManager.removeUpdates(locationListener);
             } catch (Exception e) {
             }
             DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
             drawer.closeDrawer(GravityCompat.START);
             startActivity(launchSettingsActivity);
             return true;

             // RATE APP
        } else if (id == R.id.nav_rate_app) {
             //
             Answers.getInstance().logCustom(new CustomEvent("Rate App"));
             //
             Uri uri = Uri.parse("market://details?id=" + getPackageName());
             Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
             try {
                 startActivity(myAppLinkToMarket);
             } catch (ActivityNotFoundException e) {
                 Utils.showMessage(context, "Unable to find market app");
             }

             return true;

             // SHARE APP
         } else if (id == R.id.nav_share_app) {
             //
             Answers.getInstance().logCustom(new CustomEvent("Share App"));
             //
             try {
                 Intent i = new Intent(Intent.ACTION_SEND);
                 i.setType("text/plain");
                 i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                 String sAux = "\n" + getResources().getString(R.string.share_app_text) + "\n\n";
                 sAux = sAux + "https://play.google.com/store/apps/details?id=com.liodevel.lioapp_1";
                 i.putExtra(Intent.EXTRA_TEXT, sAux);
                 startActivity(Intent.createChooser(i, "choose one"));
             } catch (Exception e) {
                 Utils.showMessage(context, "Unable to find market app");
             }

             return true;
         }

         DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    @Override
    protected void onResume() {
        Utils.logInfo("-------onResume()");
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        onlyGPS = prefs.getBoolean("only_gps", true);
        mapStyle = prefs.getString("map_style", Style.MAPBOX_STREETS);

        if (mapStyle.equals("MINIMAL")){
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6u7ln800g8b5m01lzvl7lt");
        } else if (mapStyle.equals("HILLSHADES_SAT")) {
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6y8j9300h5b5m0jyc7o5oj");
        }else {
            mapView.setStyle(mapStyle);
        }
        try {
            int tempSaveFrequency = Integer.parseInt(prefs.getString("save_frequency", "5"));
            if (tempSaveFrequency < 5){
                saveFrequency = 5;
            } else if (tempSaveFrequency > 60){
                saveFrequency = 60;
            } else {
                saveFrequency = tempSaveFrequency;
            }

        } catch (Exception e){
            saveFrequency = 5;
        }
        try {
            systemOfMeasurement = prefs.getString("system_of_measurement", "metric");
            Utils.logInfo("-PREFS- SOM : " + systemOfMeasurement);

        }catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        updateViews();
        updateGpsProviders();

    }

    @Override
    protected void onRestart() {
        Utils.logInfo("-------onRestart()");
        super.onRestart();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        onlyGPS = prefs.getBoolean("only_gps", true);
        try {
            int tempSaveFrequency = Integer.parseInt(prefs.getString("save_frequency", "5"));
            if (tempSaveFrequency < 5){
                saveFrequency = 5;
            } else if (tempSaveFrequency > 60){
                saveFrequency = 60;
            } else {
                saveFrequency = tempSaveFrequency;
            }

        } catch (Exception e){
            Crashlytics.logException(e);
            saveFrequency = 5;
        }
        try {
            systemOfMeasurement = prefs.getString("system_of_measurement", "metric");
            Utils.logInfo("-PREFS- SOM : " + systemOfMeasurement);

        }catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        updateViews();
        updateGpsProviders();

    }

    @Override
    protected void onDestroy() {
        Utils.logInfo("-------onDestroy()");
        super.onDestroy();
        try {
            timerTask.cancel();
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logInfo("Error: " + e.toString());
        }
        try {
            currentTrack.setDateEnd(lastTrackPointDate);
            currentTrack.setDistance(currentTrackDistance);
            currentTrack.save();
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }
        try {
            locationManager.removeUpdates(locationListener);
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        try {
            mNotificationManager.cancel(1);
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }


            SugarContext.terminate();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // START/STOP TRACKING
           /* case R.id.map_action_start_track:
                //clickStart(null);
                return true;
*/
            // CENTRAR MAPA
            case R.id.map_action_center_map:
                if (centerMap){
                    centerMap = false;
                    actionBarMenu.findItem(R.id.map_action_center_map).setIcon(R.drawable.ic_action_center_ko);
                } else {
                    centerMap();
                    centerMap = true;
                    actionBarMenu.findItem(R.id.map_action_center_map).setIcon(R.drawable.ic_action_center_ok);
                }
                return true;

            // MOSTRAR/OCULTAR COORDENADAS
            case R.id.map_action_show_location:
                if (showLocation){
                    showLocation(false);
                    showLocation = false;
                    actionBarMenu.findItem(R.id.map_action_show_location).setIcon(R.drawable.ic_action_communication_location_off);
                } else {
                    showLocation(true);
                    showLocation = true;
                    actionBarMenu.findItem(R.id.map_action_show_location).setIcon(R.drawable.ic_action_action_room);
                }
                return true;

            // TIPO MAPA
            case R.id.map_action_type_map:
                toggleMapType();
                return true;

            // COMPARTIR
            case R.id.map_action_share_image:
                try {
                    shareImage(getBitmapFromView(layoutToShare));
                } catch (Exception e){
                    Crashlytics.logException(e);
                    Utils.showMessage(getApplicationContext(), "Error");
                }
                return true;


            // TIPO VEHÍCULO / ACTIVIDAD
            case R.id.map_action_vehicle:
                if (!tracking) {
                    if (vehicle == 1) {
                        toggleVehicle(2);
                        vehicle = 2;
                    } else if (vehicle == 2) {
                        toggleVehicle(3);
                        vehicle = 3;
                    } else if (vehicle == 3) {
                        toggleVehicle(4);
                        vehicle = 4;
                    } else if (vehicle == 4) {
                        toggleVehicle(5);
                        vehicle = 5;
                    } else if (vehicle == 5) {
                        toggleVehicle(1);
                        vehicle = 1;
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        actionBarMenu = menu;
        getMenuInflater().inflate(R.menu.menu_actionbar_map, menu);
        //actionBarMenu.findItem(R.id.map_action_start_track).setVisible(false);
        actionBarMenu.findItem(R.id.map_action_center_map).setVisible(false);
        toggleVehicle(vehicle);
        if (!showLocation) {
            actionBarMenu.findItem(R.id.map_action_show_location).setIcon(R.drawable.ic_action_communication_location_off);
        }


        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (tracking){
                Utils.showMessage(getApplicationContext(), getResources().getString(R.string.stop_tracking));
            } else {
                if (exit) {
                    finish(); // finish activity
                } else {
                    Utils.showMessage(getApplicationContext(), getResources().getString(R.string.press_again_to_exit));
                    exit = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 3 * 1000);
                }
            }
        }
    }



    /**
     * Inicializar Mapa
     */
    private void initMapBox() {

        if (mapView == null) {
            Utils.showMessage(getApplicationContext(), getResources().getString(R.string.unable_to_create_map));
        }

        mapView.setStyle(mapStyle);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                lastLocation = location;

                infoLocationLat.setText(getResources().getString(R.string.lat) + ": " + location.getLatitude());
                infoLocationLng.setText(getResources().getString(R.string.lng) + ": " + location.getLongitude());

                if (lastLocation != null) {
                    trackerReady = true;

                    if (centerMap) {
                        LatLng auxLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                        CameraPosition camPos= new CameraPosition.Builder()
                                .target(auxLatLng)
                                .zoom(14)
                                .tilt((float)mapView.getTilt())
                                .bearing((float)mapView.getBearing())
                                .build();

                        if (mapView != null) {
                            mapView.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
                        }
                    }

                    actionBarMenu.findItem(R.id.map_action_center_map).setVisible(true);
                    if (mapView != null) {

                        if (marker == null) {
                            markerOptions = new MarkerOptions()
                                    .position(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()))
                                    .title("Hi!");
                                    //.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green));
                            marker = mapView.addMarker(markerOptions);
                        } else {
                            marker.remove();
                            markerOptions = new MarkerOptions()
                                    .position(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()))
                                    .title("Hi!");
                            //.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green));
                            marker = mapView.addMarker(markerOptions);
                        }

                        if (tracking) {

                        } else {
                            startButton.setBackgroundColor(ContextCompat.getColor(context, R.color.liodevel_red));
                            startButton.setText(getResources().getString(R.string.ready));
                            startButton.setTextSize(30);
                            // Cambiar notificación
                            mBuilder.setContentTitle(getResources().getString(R.string.tracker_ready));
                            mNotificationManager.notify(1, mBuilder.build());
                        }

                        if (centerMap) {
                            LatLng auxLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                            Utils.logInfo("-------" + auxLatLng.getLatitude() + ", " + auxLatLng.getLongitude());
                            CameraPosition camPos= new CameraPosition.Builder()
                                    .target(auxLatLng)
                                    .zoom((float)mapView.getZoom())
                                    .tilt((float)mapView.getTilt())
                                    .bearing((float) mapView.getBearing())
                                    .build();

                            if (mapView != null) {
                                mapView.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
                            }
                        }
                    }
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };

        updateGpsProviders();

    }


    /**
     * Centrar Mapa
     */
    private void centerMap(){
        LatLng auxLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        CameraPosition camPos= new CameraPosition.Builder()
                .target(auxLatLng)
                .zoom(14)
                .tilt((float) mapView.getTilt())
                .bearing((float) mapView.getBearing())
                .build();

        if (mapView != null) {
            mapView.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
        }
    }



    /**
     * Start/Stop Button
     */
    public void clickStart(View view) {
        if (!tracking) {
            // START TRACKING
            if (trackerReady) {
                Utils.logInfo("START Tracking");
                //
                Answers.getInstance().logCustom(new CustomEvent("Start tracking")
                        .putCustomAttribute("Date", (new Date().toString()))
                        .putCustomAttribute("Vehicle", String.valueOf(vehicle)));
                //
                startTrack();
                // Start track correcto
                startButton.setBackgroundColor(ContextCompat.getColor(this, R.color.liodevel_red));
                setInfosStart(true);
                centerMap();
                // Cambiar notificación
                mBuilder.setContentTitle(getResources().getString(R.string.tracking));
                mNotificationManager.notify(1, mBuilder.build());

                startButton.setText(getResources().getString(R.string.push_to_stop));
                startButton.setTextSize(30);
                tracking = true;
                chronoTrack.setBase(SystemClock.elapsedRealtime());
                chronoTrack.start();
                currentTrackDistance = 0;
                textDistanceInfo.setText("0 m");

                /*
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()),
                                18)
                );
*/
            } else {
                // GPS no preparado
                Utils.logInfo("NO GPS Ready");

            }
        } else {
            // PARAR TRACKING ?
            Utils.logInfo("STOP Tracking (Dialog)");

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder
                    .setMessage(getResources().getString(R.string.ask_stop_tracking))
                    .setPositiveButton(getResources().getString(R.string.yes),  new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            // STOP TRACKING
                            Utils.logInfo("STOP Tracking");
                            stopTimerTrack();
                            chronoTrack.stop();

                            // Cambiar notificación
                            mBuilder.setContentText("");
                            mNotificationManager.notify(1, mBuilder.build());

                            currentTrack.setDateEnd(lastTrackPointDate);
                            currentTrack.setDistance(currentTrackDistance);
                            currentTrack.setClosed(true);
                            currentTrack.save();

                            //
                            Answers.getInstance().logCustom(new CustomEvent("Stop tracking")
                                    .putCustomAttribute("Last location", lastLocation.getLatitude() + ", " + lastLocation.getLongitude())
                                    .putCustomAttribute("Distance", currentTrackDistance)
                                    .putCustomAttribute("Date", lastTrackPointDate.toString()));
                            //

                            startButton.setBackgroundColor(ContextCompat.getColor(context, R.color.liodevel_red));
                            startButton.setText(getResources().getString(R.string.ready));
                            startButton.setTextSize(30);
                            // Cambiar notificación
                            mBuilder.setContentTitle(getResources().getString(R.string.tracker_ready));
                            mNotificationManager.notify(1, mBuilder.build());
                            setInfosStart(false);
                            tracking = false;

                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    })
                    .show();

        }
    }



    // TIMERTRACK
    /**
     * Start the TimerTask
     */
    private void startTimerTrack() {
        Utils.logInfo("startTimerTrack");
        secondsTracking = 0;
        startTimemillis = System.currentTimeMillis();
        timer = new Timer();
        initializeTimerTrack();
        timer.schedule(timerTask, 0, 1000); //
    }

    /**
     * Stop the TimerTask
     */
    private void stopTimerTrack() {
        Utils.logInfo("stopTimerTrack");
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    /**
     * TimerTask for Tracking
     */
    private void initializeTimerTrack() {


        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {

                        if (counterTask == 0) {
                            Utils.logInfo("Sending location");
                            sendLocation();
                        }
                        if (counterTask == saveFrequency-1){
                            counterTask = -1;
                        }
                        Utils.logInfo("TimerTrack: " + counterTask);
                        Utils.logInfo("TimerTrack: " + chronoTrack.getText());


                        // Actualizar Notificación
                        DecimalFormat df = new DecimalFormat();
                        df.setMaximumFractionDigits(2);
                        String notifDistance = "0.0 m";
                        // MÉTRICO
                        if (systemOfMeasurement.equals("metric")) {
                            if (currentTrackDistance < 1000) {
                                notifDistance = df.format(currentTrackDistance) + " m";
                            } else {
                                notifDistance = df.format((currentTrackDistance / 1000)) + " km";
                            }
                            mBuilder.setContentText(
                                    Utils.secondsToHour((System.currentTimeMillis() - startTimemillis) / 1000)
                                            + " (" + notifDistance + ")");
                        // IMPERIAL
                        } else {
                            if (currentTrackDistance < 1000) {
                                notifDistance = df.format(currentTrackDistance / 0.9144) + " yd";
                            } else {
                                notifDistance = df.format((currentTrackDistance / 1609.3)) + " mi";
                            }
                            mBuilder.setContentText(
                                    Utils.secondsToHour((System.currentTimeMillis() - startTimemillis) / 1000)
                                            + " (" + notifDistance + ")");
                        }

                        mNotificationManager.notify(1, mBuilder.build());

                        counterTask++;
                        secondsTracking++;
                    }
                });
            }
        };
    }




    /**
     * Send startTrack
     */
    private int startTrack() {




        Utils.logInfo("SEND startTrack()");
        int ret = -1;
        mapView.removeAllAnnotations();
        currentTrackDistance = 0;
        markerOptions = new MarkerOptions()
                .position(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()))
                .title("Hi!");
                //.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red)
                //);

        if (mapView != null) {
            marker = mapView.addMarker(markerOptions);
        }

        Track track = new Track();
        track.setDate(new Date(System.currentTimeMillis()));
        track.setVehicle(vehicle);
        track.save();
        currentTrack = track;
        currentTrackObjectId = track.getId();
        startTimerTrack();
        return ret;
    }

    /**
     * Enviar trackPoint
     */
    private void sendLocation() {
        Utils.logInfo("SEND sendLocation()");

        if (lastLocation != null) {
            TrackPoint tr = new TrackPoint();
            tr.setDate(new Date(System.currentTimeMillis()));
            Utils.logInfo("TR " + lastLocation.getLongitude() + ", " + lastLocation.getLatitude());
            tr.setLat(lastLocation.getLatitude());
            tr.setLng(lastLocation.getLongitude());
            tr.setAccuracy(lastLocation.getAccuracy());
            tr.setProvider(lastLocation.getProvider());
            tr.setTrackId(currentTrackObjectId);

            if (prevLocation != null){

                trackPointDistance = prevLocation.distanceTo(lastLocation);

                if (trackPointDistance > 1) {
                    Utils.logInfo("SendTrackPoint " + trackPointDistance + "m");

                    tr.save();

                    lastTrackPointDate = tr.getDate();
                    currentTrackDistance = currentTrackDistance + trackPointDistance;
                    DecimalFormat df = new DecimalFormat();
                    df.setMaximumFractionDigits(2);
                    double kilometers;

                    if (systemOfMeasurement.equals("metric")){
                        if (currentTrackDistance < 1000) {
                            textDistanceInfo.setText(df.format(currentTrackDistance) + " m");
                        } else {
                            textDistanceInfo.setText(df.format((currentTrackDistance / 1000)) + " km");
                        }
                        kilometers = trackPointDistance / 1000.0;

                        // IMPERIAL
                    } else {
                        if (currentTrackDistance < 1000) {
                            textDistanceInfo.setText(df.format(currentTrackDistance / 0.9144) + " yd");
                        } else {
                            textDistanceInfo.setText(df.format((currentTrackDistance / 1609.3)) + " mi");
                        }
                        kilometers = trackPointDistance / 1609.3;
                    }

                    Utils.logInfo("TRACKPOINT DISTANCE      :" + trackPointDistance);
                    Utils.logInfo("TRACKPOINT DATE          :" + tr.getDate().getTime());
                    Utils.logInfo("PREVIOUSTRACKPOINT DATE  :" + previousTr.getDate().getTime());
                    long microsecs = (tr.getDate().getTime() - previousTr.getDate().getTime());
                    double hours = microsecs / 1000.0 / 3600.0;
                    double speed = kilometers / hours;
                    if (systemOfMeasurement.equals("metric")) {
                        Utils.logInfo(speed + "km/h");
                    } else {
                        Utils.logInfo(speed + "mi/h");
                    }
                    drawTrackPoint(
                            new LatLng(prevLocation.getLatitude(), prevLocation.getLongitude()),
                            new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()),
                            speed, vehicle);
                    previousTr = tr;

                    //updateSpeedChart(speed);


                } else {
                    Utils.logInfo("SendTrackPoint Skipped: " + trackPointDistance + "m");
                }

            } else {
                Utils.logInfo("FIRST SendTrackPoint");

                tr.save();
                lastTrackPointDate = tr.getDate();
                previousTr = tr;

            }
            prevLocation = lastLocation;

        } else {
            Utils.logInfo("SEND NULL Location");
        }
    }

    /**
     * Actualiza info de localización
     */
    private void updateGpsProviders() {
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        if (onlyGPS) {
            textProviderInfo.setText(getResources().getString(R.string.GPS));
        } else {
            textProviderInfo.setText(getResources().getString(R.string.gps_network));
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

        }

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception e) {
            Crashlytics.logException(e);
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception e) {
            Crashlytics.logException(e);
        }


        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(getApplicationContext().getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getApplicationContext().getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                }
            });
            dialog.setNegativeButton(getApplicationContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    finish();
                }
            });
            dialog.show();
        }


    }

    /**
     * Actualiza elementos de la pantalla al volver a esta
     */
    private void updateViews(){
        if (!trackerReady){
            startButton.setBackgroundColor(ContextCompat.getColor(this, R.color.liodevel_dark_grey));
            setInfosStart(false);
            startButton.setText(getResources().getString(R.string.getting_location));
            startButton.setTextSize(14);
            if (actionBarMenu != null) {
                //actionBarMenu.findItem(R.id.map_action_start_track).setVisible(false);
                actionBarMenu.findItem(R.id.map_action_center_map).setVisible(false);
            }

        } else {
            startButton.setBackgroundColor(ContextCompat.getColor(this, R.color.liodevel_red));
            setInfosStart(false);
            startButton.setText(getResources().getString(R.string.ready));
            startButton.setTextSize(30);
            // Cambiar notificación
            mBuilder.setContentTitle(getResources().getString(R.string.tracker_ready));
            mNotificationManager.notify(1, mBuilder.build());

            if (actionBarMenu != null) {
               // actionBarMenu.findItem(R.id.map_action_start_track).setVisible(false);
                actionBarMenu.findItem(R.id.map_action_center_map).setVisible(true);
            }
        }
        if (tracking){
            startButton.setBackgroundColor(ContextCompat.getColor(this, R.color.liodevel_red));
            setInfosStart(true);
            startButton.setText(getResources().getString(R.string.push_to_stop));
            startButton.setTextSize(30);
            //actionBarMenu.findItem(R.id.map_action_start_track).setVisible(true);
            actionBarMenu.findItem(R.id.map_action_center_map).setVisible(true);
        }

    }

    /**
     * Cambiar tipo de mapa
     */
    private void toggleMapType(){

        if (mapStyle.equals("HILLSHADES_SAT")){
            mapView.setStyle(Style.DARK);
            mapStyle = Style.DARK;

        } else if (mapStyle.equals(Style.DARK)){
            mapView.setStyle(Style.EMERALD);
            mapStyle = Style.EMERALD;

        } else if (mapStyle.equals(Style.EMERALD)){
            mapView.setStyle(Style.LIGHT);
            mapStyle = Style.LIGHT;

        } else if (mapStyle.equals(Style.LIGHT)){
            mapView.setStyle(Style.SATELLITE_STREETS);
            mapStyle = Style.SATELLITE_STREETS;

        } else if (mapStyle.equals(Style.SATELLITE_STREETS)){
            mapView.setStyle(Style.MAPBOX_STREETS);
            mapStyle = Style.MAPBOX_STREETS;

        } else if (mapStyle.equals(Style.MAPBOX_STREETS)){
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6u7ln800g8b5m01lzvl7lt");
            mapStyle = "MINIMAL";
        } else {
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6y8j9300h5b5m0jyc7o5oj");
            mapStyle = "HILLSHADES_SAT";
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("map_style", mapStyle);
        Utils.logInfo("---map_style: " + mapStyle);
        editor.apply();

    }


    /**
     * Cambiar tipo de Actividad / Vehículo
     * cambiar variable vehicle
     * icono en actionbar
     * adaptar gráfica de velocidades
     */
    private void toggleVehicle(int vehicle){
        Animation inFromRight = AnimationUtils.loadAnimation(this, R.anim.anim_in_from_right);
        Animation inFromLeft = AnimationUtils.loadAnimation(this, R.anim.anim_in_from_left);
        Animation outToRight = AnimationUtils.loadAnimation(this, R.anim.anim_out_to_right);

        if (vehicle == 2){
            Utils.logInfo("Vehicle -> 2");
            actionBarMenu.findItem(R.id.map_action_vehicle).setIcon(R.drawable.ic_motorcycle_black_36dp);
            leyenda2.bringToFront();
            leyendaColores.bringToFront();
            leyenda2.startAnimation(inFromLeft);
            leyenda1.startAnimation(outToRight);
        } else if(vehicle == 3){
            Utils.logInfo("Vehicle -> 3");
            actionBarMenu.findItem(R.id.map_action_vehicle).setIcon(R.drawable.ic_directions_bike_black_36dp);
            leyenda3.bringToFront();
            leyendaColores.bringToFront();
            leyenda3.startAnimation(inFromLeft);
            leyenda2.startAnimation(outToRight);

        } else if(vehicle == 4){
            Utils.logInfo("Vehicle -> 4");
            actionBarMenu.findItem(R.id.map_action_vehicle).setIcon(R.drawable.ic_directions_walk_black_36dp);
            leyenda4.bringToFront();
            leyendaColores.bringToFront();
            leyenda4.startAnimation(inFromLeft);
            leyenda3.startAnimation(outToRight);

        } else if(vehicle == 5){
            Utils.logInfo("Vehicle -> 5");
            actionBarMenu.findItem(R.id.map_action_vehicle).setIcon(R.drawable.ic_directions_run_black_36dp);
            leyenda5.bringToFront();
            leyendaColores.bringToFront();
            leyenda5.startAnimation(inFromLeft);
            leyenda4.startAnimation(outToRight);

        } else if(vehicle == 1){
            Utils.logInfo("Vehicle -> 1");
            actionBarMenu.findItem(R.id.map_action_vehicle).setIcon(R.drawable.ic_directions_car_black_36dp);
            leyenda1.bringToFront();
            leyendaColores.bringToFront();
            leyenda1.startAnimation(inFromLeft);
            leyenda5.startAnimation(outToRight);
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("vehicle", vehicle);
        editor.apply();

    }

    /**
     * Dibuja una linea en el mapa
     * @param start Coordenadas inicio
     * @param end Coordenadas final
     * @param speed velocidad en KM/H
     * @param vehicle 1-Coche; 2-Moto; 3-Bici; 4-Andando; 5-Corriendo
     */
    private void drawTrackPoint(LatLng start, LatLng end, double speed, int vehicle) {
        int colorTrack;

        // Coche o Moto
        if (vehicle == 1 || vehicle == 2) {
            if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 20) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 30) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 40) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 50) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 70) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 90) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 120) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }

        // Bici
        } else if (vehicle == 3) {
            if (speed < 5) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 15) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 20) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 25) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 35) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 45) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 55) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }
        // Andando
        } else if (vehicle == 4) {
            if (speed < 2) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 4) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 6) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 8) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 12) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 14) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 16) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }

        // Corriendo
        } else {
            if (speed < 5) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 15) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 20) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 25) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 30) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 35) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 40) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }
        }

        if (mapView != null) {
            PolylineOptions line =
                    new PolylineOptions().add(start, end)
                            .width(8).color(colorTrack);
            mapView.addPolyline(line);
        }
    }


    @TargetApi(21)
    private void changeNotificationBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Utils.logInfo("Notif.Bar.Coloured");
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(R.color.liodevel_dark_green));
        } else {
            Utils.logInfo("Ap");
        }
    }


    public void scaleViewUp(View v) {
        Animation scale = AnimationUtils.loadAnimation(this, R.anim.speed_animation_up);
        scale.setStartOffset(5000);
        scale.setFillAfter(true);
        v.clearAnimation();
        v.setAnimation(scale);
    }


    public void updateSpeedChart(double speed){

        Utils.logInfo("updateSpeedChart()");
        /*
        scaleView(chartBlack, 1.5f, 1.0f);
        scaleView(chartRed, 1.5f, 1.0f);
        scaleView(chartOrange, 1.5f, 1.0f);
        scaleView(chartYellow, 1.5f, 1.0f);
        scaleView(chartGreen, 1.5f, 1.0f);
        scaleView(chartDarkGreen, 1.5f, 1.0f);
        scaleView(chartBlue, 1.5f, 1.0f);
        scaleView(chartCyan, 1.5f, 1.0f);
        scaleView(chartMagenta, 1.5f, 1.0f);
*/
        if (speed < 10) {
            chartBlack.bringToFront();
            scaleViewUp(chartBlack);
        } else if (speed < 20) {
            chartRed.bringToFront();
            scaleViewUp(chartRed);
        } else if (speed < 30) {
            chartOrange.bringToFront();
            scaleViewUp(chartOrange);
        } else if (speed < 40) {
            chartYellow.bringToFront();
            scaleViewUp(chartYellow);
        } else if (speed < 50) {
            chartGreen.bringToFront();
            scaleViewUp(chartGreen);
        } else if (speed < 70) {
            chartDarkGreen.bringToFront();
            scaleViewUp(chartDarkGreen);
        } else if (speed < 90) {
            chartBlue.bringToFront();
            scaleViewUp(chartBlue);
        } else if (speed < 120) {
            chartCyan.bringToFront();
            scaleViewUp(chartCyan);
        } else {
            chartMagenta.bringToFront();
            scaleViewUp(chartMagenta);
        }
    }


    private void setInfosStart(boolean start){
        if (start) {
            textDistanceInfo.setBackground(getResources().getDrawable(R.color.liodevel_red));
            textProviderInfo.setBackground(getResources().getDrawable(R.color.liodevel_red));
            chronoTrack.setBackground(getResources().getDrawable(R.color.liodevel_red));
        } else {
            textDistanceInfo.setBackground(getResources().getDrawable(R.color.liodevel_dark_grey));
            textProviderInfo.setBackground(getResources().getDrawable(R.color.liodevel_dark_grey));
            chronoTrack.setBackground(getResources().getDrawable(R.color.liodevel_dark_grey));
        }

    }


    /**
     * Share a map image
     * @param file
     */
    private void shareImage(File file) {

        Answers.getInstance().logShare(new ShareEvent().putCustomAttribute("Screen", "Map"));
        try {

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            //shareIntent.setType("application/xml");
            shareIntent.setType("image/jpeg");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            shareIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.this_is_my_track));
            shareIntent.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.app_hashtag));
            startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));


        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.showMessage(getApplicationContext(), "Error");
            Utils.logError(e.toString());
        }

    }

    /**
     *
     * @param view
     * @return
     */
    public File getBitmapFromView(View view) {

        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());

        view.setDrawingCacheEnabled(false);
        Matrix m = new Matrix();

        // Add the SurfaceView bit (see getAllTextureViews() below)
        List<TextureView> tilingViews = ScreenshotUtil.getAllTextureViews(view);
        if (tilingViews.size() > 0) {
            Canvas canvas = new Canvas(bitmap);
            for (TextureView TextureView : tilingViews) {
                Bitmap b = TextureView.getBitmap(TextureView.getWidth(), TextureView.getHeight());
                int[] location = new int[2];
                TextureView.getLocationInWindow(location);
                int[] location2 = new int[2];
                TextureView.getLocationOnScreen(location2);
                canvas.drawBitmap(b, location[0], Utils.dpToPx(40, context), null);
                //canvas.drawBitmap(b, m , null);
            }


            canvas.drawBitmap(
                    BitmapFactory.decodeResource(getResources(), (R.mipmap.ic_launcher)),
                    0, Utils.dpToPx(40, context),
                    null);

            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setAlpha(255);
            paint.setTextSize(Utils.dpToPx(22, context));
            paint.setAntiAlias(true);
            canvas.drawText("My Tracker", Utils.dpToPx(54, context), Utils.dpToPx(66, context), paint);

            Paint paint2 = new Paint();
            paint2.setColor(Color.BLACK);
            paint2.setAlpha(255);
            paint2.setTextSize(Utils.dpToPx(22, context));
            paint2.setAntiAlias(true);
            canvas.drawText("My Tracker", Utils.dpToPx(53, context), Utils.dpToPx(65, context), paint2);


            Paint paintSub = new Paint();
            paintSub.setColor(Color.WHITE);
            paintSub.setAlpha(255);
            paintSub.setTextSize(Utils.dpToPx(16, context));
            paintSub.setAntiAlias(true);
            canvas.drawText("for Android", Utils.dpToPx(54, context), Utils.dpToPx(86, context), paintSub);

            Paint paintSub2 = new Paint();
            paintSub2.setColor(Color.BLACK);
            paintSub2.setAlpha(255);
            paintSub2.setTextSize(Utils.dpToPx(16, context));
            paintSub2.setAntiAlias(true);
            canvas.drawText("for Android", Utils.dpToPx(53, context), Utils.dpToPx(85, context), paintSub2);
        }


        Date date = new Date();

        File file = new File(Utils.getAppFolder() + date.toString().replace(" ", "_").replace(":","_") + ".png");

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Exception e){
            Crashlytics.logException(e);
        }

        return file;
    }


    private void showLocation(boolean show){
        Animation inFromUp = AnimationUtils.loadAnimation(this, R.anim.anim_in_from_up);
        Animation outToUp = AnimationUtils.loadAnimation(this, R.anim.anim_out_to_up);
        inFromUp.setFillAfter(true);
        inFromUp.setFillEnabled(true);
        outToUp.setFillAfter(true);
        outToUp.setFillEnabled(true);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("show_location", show);
        Utils.logInfo("---show_location: " + show);
        editor.apply();

        showLocation = show;

        layoutInfo.bringToFront();
        if (show){
            layoutInfoLocation.startAnimation(inFromUp);
        } else {
            layoutInfoLocation.startAnimation(outToUp);
        }


    }

}
