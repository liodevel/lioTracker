package com.liodevel.lioapp_1.Objects;

import android.location.Location;

import com.liodevel.lioapp_1.Utils.Utils;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by emilio on 19/12/2015.
 */
public class Track extends SugarRecord {

    private Date date;
    private Date dateEnd;
    private float distance;         // en metros
    private long duration;          // en segundos
    private int vehicle;            // 1-Coche; 2-Moto; 3-Bici; 4-Patinete; 5-Andando
    private String info;
    private Location fromLocation;
    private Location toLocation;
    private boolean favorite;
    private boolean closed;

    private float averageSpeed;
    private float topSpeed;

    private boolean isChecked;

    public Track() {

        date = new Date();
        dateEnd = new Date();
        distance = 0;
        fromLocation = null;
        toLocation = null;
        favorite = false;
        vehicle = 1;

        isChecked = false;
        closed = false;

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getVehicle() {
        return vehicle;
    }

    public void setVehicle(int vehicle) {
        this.vehicle = vehicle;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Location getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(Location fromLocation) {
        this.fromLocation = fromLocation;
    }

    public Location getToLocation() {
        return toLocation;
    }

    public void setToLocation(Location toLocation) {
        this.toLocation = toLocation;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public float getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(float averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public float getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(float topSpeed) {
        this.topSpeed = topSpeed;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }



}
