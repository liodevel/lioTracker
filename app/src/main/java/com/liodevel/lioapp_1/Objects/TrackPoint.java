package com.liodevel.lioapp_1.Objects;

import android.location.Location;


import com.mapbox.mapboxsdk.geometry.LatLng;
import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by emilio on 19/12/2015.
 */
public class TrackPoint extends SugarRecord {

    long trackId;
    double lat;
    double lng;
    Date date;
    float accuracy;
    String provider;

    double speed;

    public TrackPoint() {

        lat = 0.0;
        lng = 0.0;
        date = new Date();
        speed = 0.0f;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }


    public long getTrackId() {
        return trackId;
    }

    public void setTrackId(long trackId) {
        this.trackId = trackId;
    }


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
